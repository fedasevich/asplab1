using asplab1;

var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();


app.Use(async (context, next) =>
{
    context.Items["companyId"] = new Random().Next(0, 101);

    await next.Invoke(context);
});


app.MapGet("/", (context) =>
{
    int companyId = (int?)context.Items["companyId"] ?? 0;
    return context.Response.WriteAsJsonAsync(new Company(companyId, "BSNU", "68 Desantnykiv street, 10, Mykolaiv, Mykolaiv region, 54003"));
});

app.Run();